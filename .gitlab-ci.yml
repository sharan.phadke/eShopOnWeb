stages:
  - debug           
  - restore 
  - build
  - test
  - package
  - cleanup

variables:
  NUGET_PACKAGES: "$CI_PROJECT_DIR/.nuget/packages"
  GIT_DEPTH: 5

restore-job:
  image: mcr.microsoft.com/dotnet/sdk:7.0
  stage: restore
  script:
    - dotnet restore eShopOnWeb.sln --packages $NUGET_PACKAGES

cache:
  key:  "$CI_COMMIT_REF_SLUG"
  paths:
    - $NUGET_PACKAGES/

dotnet-build-job-web:
  image: mcr.microsoft.com/dotnet/sdk:7.0
  stage: build
  script:
    - echo "Compiling the code..."
    - cd src/Web
    - dotnet build 
    - echo "Compile complete."

dotnet-build-job-api:
  image: mcr.microsoft.com/dotnet/sdk:7.0
  stage: build
  script:
    - echo "Compiling the code..."
    - cd src/PublicApi
    - dotnet build 
    - echo "Compile complete."


dotnet-unit-test-job:       # This job runs in the build stage, which runs first.
  stage: test
  image: mcr.microsoft.com/dotnet/sdk:7.0
  script:
    - echo "Running Tests...."
    - cd tests/UnitTests
    - dotnet test 
    - echo "Testing complete."

dotnet-integration-test-job:       # This job runs in the build stage, which runs first.
  stage: test
  image: mcr.microsoft.com/dotnet/sdk:7.0
  script:
    - echo "Running Tests...."
    - cd tests/IntegrationTests
    - dotnet test 
    - echo "Integration Testing complete."

dotnet-functional-test-job:       # This job runs in the build stage, which runs first.
  stage: test
  image: mcr.microsoft.com/dotnet/sdk:7.0
  script:
    - echo "Running Tests...."
    - cd tests/FunctionalTests
    - dotnet test 
    - echo "Testing complete."

dotnet-api-integration-test-job:       # This job runs in the build stage, which runs first.
  stage: test
  image: mcr.microsoft.com/dotnet/sdk:7.0
  script:
    - echo "Running Tests...."
    - cd tests/PublicApiIntegrationTests
    - dotnet test 
    - echo "Integration Testing complete."

package-web-job:
  stage: package
  script:
    - cd src/Web
    - dotnet publish -c Release -o web_out
  artifacts:
    expire_in: 1 day 
    paths:
      - ./src/Web/web_out

package-api-job:
  stage: package
  script:
    - cd src/PublicApi
    - dotnet publish -c Release -o api_out
  artifacts:
    expire_in: 1 day 
    paths:
      - ./src/PublicApi/api_out

lint-job:
  stage: test
  image: mcr.microsoft.com/dotnet/sdk:6.0
  script:
    - dotnet tool install -g roslynator.dotnet.cli
    - export PATH="$PATH:/root/.dotnet/tools"
    - source ~/.bashrc
    - mkdir results
    - roslynator analyze eShopOnWeb.sln -o ./results/roslyn-result.xml || true
  artifacts:
    expire_in: 1 week
    expose_as: 'SAST-report' 
    paths:
    - ./results

code-coverage-job:
  stage: test
  image: mcr.microsoft.com/dotnet/sdk:7.0
  script:
    - dotnet tool install --global dotnet-reportgenerator-globaltool --version 5.1.22
    - export PATH="$PATH:/root/.dotnet/tools"
    - source ~/.bashrc
    - dotnet test eShopOnWeb.sln --collect:"XPlat Code Coverage" --results-directory TestResults --logger trx
    - coverage_xml_path=$(find TestResults -name "coverage.cobertura.xml" -type f -print -quit)
    - reportgenerator "-reports:$coverage_xml_path" "-targetdir:TestResults/Coverage" "-reporttypes:Html"
  artifacts:
    expire_in: 1 week
    expose_as: 'code-coverage-report' 
    paths:
      - ./TestResults

docker-volume-cleanup-job:      # This job runs in the deploy stage.
  stage: cleanup  # It only runs when *both* jobs in the test stage complete successfully.
  image: docker
  script:
    - echo "Cleaning unwanted docker volumes"
    - docker volume rm $(docker volume ls -qf dangling=true) || true
    - echo "Cleanup Complete!"